require('isomorphic-fetch');
const Koa = require('koa');
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const dotenv = require('dotenv');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const session = require('koa-session');

dotenv.config();
const { default: graphQLProxy } = require('@shopify/koa-shopify-graphql-proxy');
const { ApiVersion } = require('@shopify/koa-shopify-graphql-proxy');


const port = 3001;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY } = process.env;
app.prepare().then(() => {
    const server = new Koa();
    server.use(session({ sameSite: 'none', secure: true }, server));
    server.keys = [SHOPIFY_API_SECRET_KEY];

    server.use(
        createShopifyAuth({
            // prefix: '/shopify',
            apiKey: SHOPIFY_API_KEY,
            secret: SHOPIFY_API_SECRET_KEY,
            scopes: ['read_products', 'write_products','read_orders','write_orders'],
            accessMode: 'offline',
            afterAuth(ctx) {
                const urlParams = new URLSearchParams(ctx.request.url);
                const shop = urlParams.get('shop');
                ctx.redirect(`/?shop=${shop}`);
            },
        }),
    );
    server.use(graphQLProxy({ version: ApiVersion.October19 }))
    server.use(verifyRequest());
    server.use(async (ctx) => {
        const {accessToken} = ctx.session;
        ctx.res.token = accessToken

        await handle(ctx.req, ctx.res);
        ctx.respond = false;
        ctx.res.statusCode = 200;
    });

    server.listen(port, () => {
        console.log(`> Ready on http://localhost:${port}`);
    });
});

// require('isomorphic-fetch');

// const Koa  = require('koa');
// const session   = require('koa-session');
// // const shopifyAuth =  require('@shopify/koa-shopify-auth');
// const shopifyAuth = require('node-shopify-auth');
// const { verifyRequest } = require('@shopify/koa-shopify-auth');

// const {SHOPIFY_API_KEY, SHOPIFY_SECRET} = process.env;

// const app = new Koa();
// app.keys = [SHOPIFY_SECRET];

// shopifyAuth.setOptions({
//     apiKey: SHOPIFY_API_KEY,
//     secret: SHOPIFY_SECRET,
//     scopes: ['write_orders, write_products'],
//     afterAuth(ctx) {
//       const {shop, accessToken} = ctx.session;

//       console.log('We did it!', accessToken);

//       ctx.redirect('/');
//     },
// });
// app.use(session({secure: true, sameSite: 'none'}, app));
// app.use('/',ShopifyAuth.authenticate());
// app.use(verifyRequest());
// app.use(ctx => {
//     ctx.body = 'd';
// });